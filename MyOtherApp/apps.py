from django.apps import AppConfig


class MyotherappConfig(AppConfig):
    name = 'MyOtherApp'
